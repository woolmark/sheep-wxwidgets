#include <iostream>

#include "sheep.h"

class SheepApp: public wxApp {
public:
	virtual bool OnInit();
	virtual int OnExit();
private:
	SheepPanel* panel;
};

bool SheepApp::OnInit() {
	auto frame = new wxFrame(nullptr, wxID_ANY, wxT("sheep"), wxPoint(-1, -1), wxSize(120, 120));

	panel = new SheepPanel(frame);
	if (panel) {
		panel->Start();

		frame->Show(true);
		return true;
	} else {
		return false;
	}
}

int SheepApp::OnExit() {
	if (panel) {
		panel->Stop();
	}
	return 0;
}

// Initialize and start wxApp instead of main
wxIMPLEMENT_APP(SheepApp);

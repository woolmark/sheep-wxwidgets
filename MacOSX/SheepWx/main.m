//
//  main.m
//  SheepWx
//
//  Created by TakimuraNaoki on 1/4/16.
//  Copyright © 2016 TakimuraNaoki. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}

#include "sheep.h"
#include "sheep_img.h"
#include <wx/mstream.h>
#include <iostream>

static const int DEFAULT_FRAME_WIDTH = 120;

static const int DEFAULT_FRAME_HEIGHT = 120;

static const int SHEEP_MOVE_X_DELTA = 5;

static const int SHEEP_MOVE_Y_DELTA = 3;

wxDEFINE_EVENT(EVT_UPDATE, wxCommandEvent);

SheepPanel::SheepPanel(wxWindow* parent) :
		wxPanel(parent), wxThread(wxTHREAD_JOINABLE),
		running(false), scale(0), count(0),
		sheep00Bitmap(nullptr), sheep01Bitmap(nullptr), fenceBitmap(nullptr) {

	wxImage::AddHandler(new wxPNGHandler());

	sheepFlock = new wxVector<Sheep>();

	sheep00Image = new wxImage(
			*(new wxMemoryInputStream(sheep00_png, sheep00_png_len)));
	sheep00Image->InitAlpha();

	sheep01Image = new wxImage(
			*(new wxMemoryInputStream(sheep01_png, sheep01_png_len)));
	sheep01Image->InitAlpha();

	fenceImage = new wxImage(
			*(new wxMemoryInputStream(fence_png, fence_png_len)));
	fenceImage->InitAlpha();

	Bind(wxEVT_SIZE, &SheepPanel::OnSize, this);
	Bind(wxEVT_PAINT, &SheepPanel::OnPaint, this);
	Bind(EVT_UPDATE, &SheepPanel::OnUpdate, this);
}

void SheepPanel::Start() {

    wxThreadError result = wxThread::Run();
	switch (result) {
	case wxTHREAD_NO_ERROR:
		running = true;
		break;
	case wxTHREAD_RUNNING:
		break;
	case wxTHREAD_NO_RESOURCE:
		running = false;
		break;
	default:
		running = false;
		break;
	}

}

void SheepPanel::Stop() {
	running = false;
}

void SheepPanel::OnSize(wxSizeEvent& event) {

	wxSize size = GetSize();

	int scale;
	int ratioWidth = size.x / DEFAULT_FRAME_WIDTH;
	int ratioHeight = size.y / DEFAULT_FRAME_HEIGHT;
	if (ratioWidth > ratioHeight) {
		scale = ratioHeight;
	} else {
		scale = ratioWidth;
	}
	if (scale <= 0) {
		scale = 1;
	}

	if (this->scale != scale) {
		this->scale = scale;

		sheep00Bitmap = GetScaledBitmap(*sheep00Image, scale);
		sheep01Bitmap = GetScaledBitmap(*sheep01Image, scale);
		fenceBitmap = GetScaledBitmap(*fenceImage, scale);
	}

	fenceSize.x = FENCE_WIDTH * scale;
	fenceSize.y = FENCE_HEIGHT * scale;
	fencePoint.x = (size.x - fenceSize.x) / 2;
	fencePoint.y = size.y - fenceSize.y;

	groundSize.x = size.x;
	groundSize.y = (int) (fenceSize.y * 0.9f);
	groundPoint.x = 0;
	groundPoint.y = size.y - groundSize.y;

	if (sheepFlock) {
		delete sheepFlock;
	}
	sheepFlock = new wxVector<Sheep>();

}

wxBitmap* SheepPanel::GetScaledBitmap(const wxImage& image, int scale) {
	wxSize size = image.GetSize();
	return new wxBitmap(image.Scale(size.x * scale, size.y * scale));
}

void SheepPanel::OnUpdate(wxCommandEvent& event) {

	if (scale <= 0) {
		return;
	}

	auto newSheepFlock = new wxVector<Sheep>();

	for (auto sheep : *sheepFlock) {
		sheep.Run();
		if (sheep.IsJumpUp()) {
			count++;
		}
		if (!sheep.IsGoAway()) {
			newSheepFlock->push_back(sheep);
		}
	}

	if (newSheepFlock->size() <= 0) {
		newSheepFlock->push_back(Sheep(*this, scale));
	}

	if (sheepFlock) {
		delete sheepFlock;
	}
	sheepFlock = newSheepFlock;

	Refresh();
}

void SheepPanel::OnPaint(wxPaintEvent& event) {

	if (groundPoint.x < 0 || groundPoint.y < 0 || groundSize.x <= 0 || groundSize.y <= 0) {
		return;
	}

	wxPaintDC dc(this);
	dc.SetPen(*wxTRANSPARENT_PEN);

	dc.SetBrush(SKY_COLOUR);
	dc.DrawRectangle(wxPoint(0, 0), GetSize());

	dc.SetBrush(GROUND_COLOUR);
	dc.DrawRectangle(groundPoint, groundSize);

	if (fenceBitmap) {
		dc.DrawBitmap(*fenceBitmap, fencePoint);
	}
    
    if (sheep00Bitmap && sheep01Bitmap) {
        for (auto sheep : *sheepFlock) {
            if (sheep.IsStretchLeg()) {
                dc.DrawBitmap(*sheep01Bitmap, sheep.GetPoint());
            } else {
                dc.DrawBitmap(*sheep00Bitmap, sheep.GetPoint());
            }
        }
    }

	dc.DrawText(wxString::Format("%d sheep", count), 5, 5);
}

wxThread::ExitCode SheepPanel::Entry() {

	while (running) {
		wxPostEvent(this, wxCommandEvent(EVT_UPDATE));

		Sleep(100);
	}

	return 0;
}

Sheep::Sheep(SheepPanel& panel) : Sheep::Sheep(panel, 1) {}

Sheep::Sheep(SheepPanel& panel, const int scale) :
    width(SHEEP00_WIDTH * scale),
    height(SHEEP00_HEIGHT * scale),
    stretchLeg(false),
    jumpState(0),
	scale(scale)
{
	assert(scale > 0);

	auto pSize = panel.GetSize();
	auto gSize = panel.GetGroundSize();
	auto fSize = panel.GetFenceSize();

	x = pSize.x + width;
	y = pSize.y - random() % gSize.y - height;

	jumpX = -1 * (y - pSize.y) * fSize.x / fSize.y + (pSize.x - fSize.x) / 2;
}

void Sheep::Run() {

	x -= SHEEP_MOVE_X_DELTA * scale;

	if (x < jumpX && jumpState < 3) {
		y -= SHEEP_MOVE_Y_DELTA * scale;
		stretchLeg = true;

		jumpState++;
	} else if (x < jumpX && jumpState < 6) {
		y += SHEEP_MOVE_Y_DELTA * scale;
		stretchLeg = true;

		jumpState++;
	} else {
		stretchLeg = !stretchLeg;
	}

}

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

class Sheep;

class SheepPanel: public wxPanel, public wxThread {
public:

	SheepPanel(wxWindow *parent);

	int GetCount() { return count; }

	int GetScale() { return scale; }

	wxPoint getGroundPoint() { return groundPoint; }

	wxSize GetGroundSize() { return groundSize; }

	wxPoint getFencePoint() { return fencePoint; }

	wxSize GetFenceSize() { return fenceSize; }

	wxSize GetSheepSize() { return fenceSize; }

	void Start();

	void Stop();

private:
	bool running;

	int scale;

	int count;

	wxPoint groundPoint;

	wxSize groundSize;

	wxPoint fencePoint;

	wxSize fenceSize;

	wxSize sheepSize;

	wxVector<Sheep>* sheepFlock;

	wxImage* sheep00Image;

	wxImage* sheep01Image;

	wxImage* fenceImage;

	wxBitmap* sheep00Bitmap;

	wxBitmap* sheep01Bitmap;

	wxBitmap* fenceBitmap;

	wxColour SKY_COLOUR = wxColour(150, 150, 255);

	wxColour GROUND_COLOUR = wxColour(100, 255, 100);

	wxBitmap* GetScaledBitmap(const wxImage& image, int scale);

	wxThread::ExitCode Entry();
	void OnUpdate(wxCommandEvent& event);
	void OnPaint(wxPaintEvent& event);
	void OnSize(wxSizeEvent& event);

};

class Sheep {
public:

	Sheep(SheepPanel& panel);

	Sheep(SheepPanel& panel, const int scale);

	wxPoint GetPoint() {
		return wxPoint(x, y);
	}

	wxSize GetSize() {
		return wxSize(width, height);
	}

	bool IsStretchLeg() {
		return stretchLeg;
	}

	int GetJumpX() {
		return jumpX;
	}

	int GetJumpState() {
		return jumpState;
	}

	int GetScale() {
		return scale;
	}

	bool IsJumpUp() {
		return (jumpState == 3);
	}

	bool IsGoAway() {
		return (x < -1 * width);
	}

	void Run();

private:
	int x;
	int y;
	int width;
	int height;

	bool stretchLeg;

	int jumpX;

	int jumpState;

	int scale;

};
